<?php
require_once ("./classes/database.class.php");

require_once ("./classes/adherent.class.php");
require_once ("./classes/adherentManager.class.php");

require_once ("./classes/loginManager.class.php");

// Fonction de connexion
function login($login,$pass){
    $manager = new loginManager(database::getDB());
    $token = $manager->checkLogin($login,$pass);
    if(!$token) //si le token est false
        echo '{"status":"error","token":"null"}';
    else
        echo '{"status":"logged","token":"'.$token.'"}';
}

function checkToken($token){
    $manager = new loginManager(database::getDB());
    return $manager->checkToken($token);
}


// Retourne la liste de tous les adherents
function getAdherents(){
    $manager = new adherentManager(database::getDB());
    $tabAdherents = $manager->getList();
    echo json_encode($tabAdherents);
}

/////////////////////a coder ///////////////////
// Retourne le client dont l'id est passé en paramètre au format JSON.
function getClientId($id){
    $manager = new clientManager(database::getDB());
    $client = $manager->get($id);
    echo json_encode($client);
}

//retourne les clients correspondant au nom est passé en paramètre
function getClientsNom($nom){
    $manager = new clientManager(database::getDB());
    $tabClients = $manager->getList("WHERE nomCli LIKE '%".$nom."%'");
    echo json_encode($tabClients);
}

//retourne la liste de toutes les commandes pour un client donné
function getAdherentsParSection($section){
    $manager = new adherentManager(database::getDB());
    $tabAdherents = $manager->getList("WHERE sectionAdherent='".$section."'");
    echo json_encode($tabAdherents);
}

