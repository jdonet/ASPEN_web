<?php
require_once("facade.php");

if ($_SERVER["REQUEST_METHOD"]=="POST"){
    $PARRAY = $_POST;
}
else {
    $PARRAY = $_GET;
}

/*selection de l'action */
if (isset($PARRAY["action"]))
{
    $act=$PARRAY["action"];
    
        if($act=="login"){
                login($PARRAY["login"],$PARRAY["pass"]);
        }elseif(isset($PARRAY["token"]) && checkToken($PARRAY["token"])){ //require valid token
                if($act=="getAdherents"){
                        getAdherents();
                }else if($act=="getAdherentId"){
                        getAdherentId($PARRAY["id"]);
                }else if($act=="getAdherentsNom"){
                        getAdherentsNom($PARRAY["nom"]);
                }else if ($act=="getAdherentsParSection"){
                        getAdherentsParSection($PARRAY["section"]);
                }else{
                        echo("Action inconnue !");
                }
        }else{
                echo ("Erreur d authentification");
        }
        		
}
else{
        echo ("Erreur, vérifiez les paramètres de votre requête http !");
}


