<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les authentifications.
 *
 * @author Julien
 */
class loginManager {
    
    private $db;
    
    /**
     * Instancie un objet adherentManager.
     * 
     * Permet d'instancier un objet adherentManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    
    
    /**
     * Vérifie l'authentification dans la base.
     * 
     * Méthode qui vérifie renvoie un token si authentification ok.
     * 
     * @param string login recherché
     * @param string pass recherché
     * @return string|boolean Renvoie le token ou FALSE si erreur de connexion
     */
    public function checkLogin($login,$pass)
    {
        $query = "select * from `%sauthentification%s` WHERE `login`=? AND `password`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$login);
            $traitement->bindparam(2,md5($pass));
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            
            //on vérifie que le compte est encore valide
            if ((int)$row['active']===1){
                $token = md5(uniqid($login, true));
                $query = "insert into `%stoken%s` (`token`, `token_generated`,`token_validity`) values (?,?,?);";
                $traitement = $this->db->prepare($query);
                $param1=$token;
                $traitement->bindparam(1,$param1);
                $param2=date("Y-m-d H:i:s");
                $traitement->bindparam(2,$param2);
                $param3=date("Y-m-d H:i:s",mktime(date("H") +1, date("i"), date("s"), date("m")  , date("d"), date("Y"))); //dans une heure
                $traitement->bindparam(3,$param3);
                $traitement->execute();
                return $token;
            }else{
                return false;    
            }
        }
        else {
            return false;
        }
    }

    /**
     * Vérifie la validité du token dans la base.
     * 
     * @param string token recherché
     * @return boolean Renvoie TRUE ou FALSE si erreur de connexion
     */
    public function checkToken($token)
    {
        $query = "select * from `%stoken%s` WHERE `token`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$token);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            //on vérifie que le token est encore valide
            if (strtotime($row['token_validity']) >= strtotime(date("Y-m-d H:i:s"))){
                return true;
            }else{
                return false;    
            }
        }
        else {
            return false;
        }
    }
}