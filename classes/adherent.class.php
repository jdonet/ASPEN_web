<?php

class adherent implements JsonSerializable {
    private $id="";
    private $nom="";
    private $prenom="";
    private $mail="";
    private $telephone="";
    private $paye="";
    private $section="";

    
    /**
     * Instancie un objet adherent.
     *  
     * @param string Nom du adherent.
     * @param string Prénom du adherent.
     * @param string Mail du adherent.
     * @param string Tel du adherent.
     * @param string Paye adherent.
     * @param string Section du adherent.
     * 
     */
    public function __construct($nom, $pre, $mail, $tel, $paye, $section){
        $this->nom = $nom;
        $this->prenom = $pre;
        $this->mail = $mail;
        $this->tel = $tel;
        $this->paye = $paye;
        $this->section = $section;

    }
    
    // Mutateurs chargés de modifier les attributs
    public function setId($id){$this->id = $id;}    
    public function setNom($n){$this->nom = $n;}
    public function setPrenom($p){$this->prenom = $p;}
    public function setMail($m){$this->mail = $m;}
    public function setTelephone($t){$this->tel = $t;}
    public function setPaye($p){$this->paye = $p;}
    public function setSection($s){$this->section = $s;}

    
    // Accesseurs chargés d'exposer les attributs
    public function getId(){return $this->id;}
    public function getNom(){return $this->nom;}
    public function getPrenom(){return $this->prenom;}
    public function getMail(){return $this->mail;}
    public function getTelephone(){return $this->telephone;}
    public function getPaye(){return $this->paye;}
    public function getSection(){return $this->section;}

    /**
     * Spécifie les données qui doivent être linéarisées en JSON.
     * 
     * Linéarise l'objet en une valeur qui peut être linéarisé nativement par la fonction json_encode().
     * 
     * @return mixed Retourne les données qui peuvent être linéarisées par la fonction json_encode()
     */
    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'prenom' => $this->prenom,
            'mail' => $this->mail,
            'telephone' => $this->telephone,
            'paye' => $this->paye,
            'section' => $this->section
        ];
    }
}