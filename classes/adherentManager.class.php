<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les adherents.
 *
 * @author Julien
 */
class adherentManager {
    
    private $db;
    
    /**
     * Instancie un objet adherentManager.
     * 
     * Permet d'instancier un objet adherentManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    /**
     * Enregistre ou Modifie l adherent dans la base.
     * 
     * Pour enregistrer l adherent passé en paramètre en base de données :
     *      UPDATE si l adherent est déjà existant;
     *      INSERT sinon (si id non trouvé ou non spécifié).
     * 
     * @param produit Adherent à enregister ou mettre à jour.
     * 
     * @return int Retourne l'id du adherent ajouté ou mis à jour.
     */
    public function save(adherent $adh)
    {        
        $nbRows = 0;

        // le secteur que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($adh->getId()!=''){
            $query = "select count(*) as nb from `%sadherent%s` where `idAdherent`=?";
            $traitement = $this->db->prepare($query);
            $param1=$adh->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si le secteur que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `%sadherent%s` set `nomAdherent`=?, `prenomAdherent`=?, `rueAdherent`=?, `cpAdherent`=?, `villeAdherent`=? where `idAdherent`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$adh->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$adh->getPrenom();
            $traitement->bindparam(2,$param2);
            $param3=$adh->getRue();
            $traitement->bindparam(3,$param3);
            $param4=$adh->getCP();
            $traitement->bindparam(4,$param4);
            $param5=$adh->getVille();
            $traitement->bindparam(5,$param5);
            $param6=$adh->getId();
            $traitement->bindparam(6,$param6);
            $traitement->execute();
        }
        // sinon : INSERT
        else
        {
            $query = "insert into `%sadherent%s` (`nomAdherent`, `prenomAdherent`,`rueAdherent`,`cpAdherent`,`villeAdherent`) values (?,?,?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$adh->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$adh->getPrenom();
            $traitement->bindparam(2,$param2);
            $param3=$adh->getRue();
            $traitement->bindparam(3,$param3);
            $param4=$adh->getCP();
            $traitement->bindparam(4,$param4);
            $param5=$adh->getVille();
            $traitement->bindparam(5,$param5);            
            $traitement->execute();
        }
        
        if ($adh->getId() == "")
        {
            $adh->setId($this->db->lastInsertId());
        }
        return $adh->getId();
    }

    /**
     * Supprime le adherent de la base.
     * 
     * Supprime de la base le adherent (table "adherent"), les commandes (table "commande") passées par celui-ci et les lignes de commandes associées (table "comporter").
     * 
     * @param produit Objet adherent devant être supprimé.
     * @return boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */    
    public function delete(adherent $adh)
    {
        $nbRows = 0;

        // le adherent que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($adh->getId()!=''){                    
            $query = "select count(*) as nb from `%sadherent%s` where `idAdherent`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $adh->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI le adherent que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM comporter, commande, adherent
        //          et retourne TRUE
        if ($nbRows > 0)
        {            
            
            // DELETE FROM presence
            $query = "DELETE FROM `presence` WHERE idAdherent=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $adh->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            // DELETE FROM adherent
            $query = "DELETE FROM `%sadherent%s` WHERE idAdherent=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $adh->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }

    /**
     * Sélectionne un(des) adherent(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de adherent correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir tous les adherents.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) adherent.
     */
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `%sadherent%s` ".$restriction.";";
        $adhList = Array();

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            //appel du constructeur paramétré
            $adh = new adherent($row['nomAdherent'],$row['prenomAdherent'],$row['mailAdherent'],$row['telephoneAdherent'],$row['payeAdherent'],$row['sectionAdherent']);
            //positionnement de l'id
            $adh->setId($row['idAdherent']);
            //ajout de l'objet à la fin du tableau
            $adhList[] = $adh;
        }
        //retourne le tableau d'objets 'adherent'
        return $adhList;   
    }
    
    /**
     * Sélectionne un adherent dans la base.
     * 
     * Méthode de SELECT qui renvoie le adherent dont l'id est spécifié en paramètre.
     * 
     * @param int ID du adherent recherché
     * @return produit|boolean Renvoie l'objet adherent recherché ou FALSE s'il n'a pas été trouvé
     */
    public function get($id)
    {
        $query = "select * from `%sadherent%s` WHERE `idAdherent`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            //On instancie un objet 'adherent' avec les valeurs récupérées
            //appel du constructeur paramétré
            $adh = new adherent($row['nomAdherent'],$row['prenomAdherent'],$row['mailAdherent'],$row['telephoneAdherent'],$row['payeAdherent'],$row['sectionAdherent']);
            //positionnement de l'id
            $adh->setId($row['idAdherent']);

            //retourne l'objet 'adherent' correpsondant
            return $adh;
        }
        else {
            return false;
        }
    }
    
}